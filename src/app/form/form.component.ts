import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { phoneValidator } from '../validate-phone.derective';
import { FormModel, Skill } from '../shared/form.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormService } from '../shared/form.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  reactiveForm!: FormGroup;
  formData: FormModel | null = null;
  postFormDataSubscription!: Subscription;
  loading!: boolean;
  isEdit = false;
  editedId = '';
  textAreaLength: number = 300;
  isOpen = true;
  count = 0;

  constructor(
    private formService: FormService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.reactiveForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      patronymic: new FormControl('', Validators.required),
      number: new FormControl('',[Validators.required, phoneValidator]),
      place: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      size: new FormControl('', Validators.required),
      comment: new FormControl('', Validators.required),
      skills: new FormArray([])
    });
    this.postFormDataSubscription = this.formService.addFormDataLoading.subscribe( (isLoading:boolean) => {
      this.loading = isLoading;
    });
    this.route.data.subscribe(data => {
      const formData = <FormModel | null>data.formData;
      this.formData = formData;

      if (formData) {
        this.isEdit = true;
        this.editedId = formData.id;
        this.setFormValue({
          firstName: formData.firstName,
          lastName: formData.lastName,
          patronymic: formData.patronymic,
          number: formData.number,
          place: formData.place,
          gender: formData.gender,
          size: formData.size,
          skills: [],
          comment: formData.comment,
        });
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          firstName: '',
          lastName: '',
          patronymic: '',
          number: '',
          place: '',
          gender: '',
          size: '',
          skills: [],
          comment: '',
        });
      }
    })
  }

  setFormValue(value: { [key: string]: any }) {
    setTimeout(() => {
      this.reactiveForm.setValue(value);
    });
  }

  onSubmit() {
    const id = this.editedId || Math.random().toString();
    const formData = new FormModel(
      id, this.reactiveForm.value.firstName,
      this.reactiveForm.value.lastName,
      this.reactiveForm.value.patronymic,
      this.reactiveForm.value.number,
      this.reactiveForm.value.place,
      this.reactiveForm.value.gender,
      this.reactiveForm.value.size,
      this.reactiveForm.value.skills,
      this.reactiveForm.value.comment);
    const next = () => {
      this.formService.fetchAllFormData();
    }
    if (this.isEdit) {
      this.formService.editFormData(formData).subscribe(next);
      void this.router.navigate(['edit']);
    } else {
      this.formService.addFormData(formData).subscribe(next);
      void this.router.navigate(['saved']);
    }
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.reactiveForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  fieldSkillsError(fieldName: string, index: number, errorType: string) {
    const skills =<FormArray>this.reactiveForm.get('skills');
    const field = skills.controls[index].get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  creatSkills(data: Skill) {
    const skills = <FormArray>this.reactiveForm.get('skills');
    const skill = new FormGroup({
      skill: new FormControl(`${data.skill}`, Validators.required),
      level: new FormControl(`${data.level}`, Validators.required),
    });
    skills.push(skill);
  }

  addSkill() {
    if (this.isEdit) {
      if (this.formData?.skills.length !== this.count) {
        this.formData?.skills.forEach( (data: Skill) => {
          this.count = <number>this.formData?.skills.length;
          this.creatSkills(data);
          this.isOpen = false;
        });
      } else {
        this.creatSkills({skill:'', level: ''});
        this.isOpen = false;
      }
    } else  {
      this.creatSkills({skill:'', level: ''});
      this.isOpen = false;
    }
  }

  getSkill() {
    return (<FormArray> this.reactiveForm.get('skills')).controls
  }

  onCountingTheNumberOfCharacters() {
    this.textAreaLength = 300;
    this.textAreaLength  = this.textAreaLength - this.reactiveForm.value.comment.length;
  }
}
