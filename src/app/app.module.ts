import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { ToolbarComponent } from './ui/toolbar/toolbar.component';
import { FooterComponent } from './ui/footer/footer.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ValidatePhoneDirective } from './validate-phone.derective';
import { AllFormDataComponent } from './all-form-data/all-form-data.component';
import { FormDataComponent } from './form-data/form-data.component';
import { NotFoundComponent } from './not-found.component';
import { SaveNewFormDataComponent } from './save-new-form-data.component';
import { EditFormComponent } from './edit-form.component';
import { HttpClientModule } from '@angular/common/http';
import { FormService } from './shared/form.service';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    ToolbarComponent,
    FooterComponent,
    ValidatePhoneDirective,
    AllFormDataComponent,
    FormDataComponent,
    NotFoundComponent,
    SaveNewFormDataComponent,
    EditFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [FormService],
  bootstrap: [AppComponent]
})
export class AppModule { }
